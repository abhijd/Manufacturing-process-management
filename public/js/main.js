
$(document).ready(function() {


		//**********File Preparing************
		var files;
		// Add events
		$('input[type=file]').on('change', prepareUpload);
		// Grab the files and set them to our variable
		function prepareUpload(event)
		{
		  files = event.target.files;
		  // console.log(files[0]);
		}
		//**********End of File Preparing*******


	$('#xmlSubmit').click(function (event) {

		event.preventDefault();

		var data = new FormData();
			 file=files[0];
			 filename=file.name;
		    data.append('xmlFile', file);
		    // console.log(data);
		    
		    callAjax('/checkXML',data);
		    $("#StatusMessage").html($("#StatusMessage").html()+"<br><br><br><br>&#9731;&#9731;&#9731;&#9731;&#9731;<br> Processing your request, please wait.<br>&#9731;&#9731;&#9731;&#9731;&#9731;");
		    $('#IndexStatusSection2 .row div').animate({ scrollTop: $('#StatusMessage').height() }, 700);
	});



	$('#FormOrderSubmit').click(function (event) {

		event.preventDefault();
		var date= new Date();

		var mm=date.getMonth();
		mm++;
		var d=date.getFullYear()+'-'+mm+'-'+date.getDate();

		var convertedToJason={
 
							    "order_details": {
							     
							        "ordered_by": {
							            "name": {
							                "_text": $("#nameForm").val()
							            },
							            "email": {
							                "_text": $("#emailForm").val()
							            },
							            "phone": {
							                "_text": $("#phoneForm").val()
							            },
							            "address": {
							                "line1": {
							                    "_text": $("#line1Form").val()
							                },
							                "line2": {
							                    "_text": $("#line2Form").val()
							                },
							                "city": {
							                    "_text": $("#cityForm").val()
							                },
							                "country": {
							                    "_text": $("#countryForm").val()
							                }
							            }
							        },
							        "order_id": {
							            "_text": $("#IDForm").val()
							        },
							        "order_date": {
							            "_text": d
							        },
							        "ordered_items": {
							            "item": [
							                {
							                    "name": {
							                        "_text": $("#productNameForm").val()
							                    },
							                    "quantity": {
							                        "_text": $("#quantityForm").val()
							                    },
							                    "frame": {
							                        "design": {
							                            "_text": $("#frameTypeForm").val()
							                        },
							                        "color": {
							                            "_text": $("#frameColorForm").val()
							                        }
							                    },
							                    "screen": {
							                        "design": {
							                            "_text": $("#screenTypeForm").val()
							                        },
							                        "color": {
							                            "_text": $("#screenColorForm").val()
							                        }
							                    },
							                    "keypad": {
							                        "design": {
							                            "_text": $("#keypadTypeForm").val()
							                        },
							                        "color": {
							                            "_text": $("#keypadColorForm").val()
							                        }
							                    }
							                }
							            ]
							        }
							    }
							};

			console.log(convertedToJason);
			var data=JSON.stringify(convertedToJason);
			callAjax('/peeppeepOrderTule',data);
			$("#StatusMessage").html($("#StatusMessage").html()+"<br><br><br><br>&#9731;&#9731;&#9731;&#9731;&#9731;<br> Processing your request, please wait.<br>&#9731;&#9731;&#9731;&#9731;&#9731;");
			$('#IndexStatusSection2 .row div').animate({ scrollTop: $('#StatusMessage').height() }, 700);
	});


	
	$('#StatOrderSubmit').click(function (event) {

		event.preventDefault();

		var name=$('#nameStat').val();
		var phone=$('#phoneStat').val();
		var temp={'name': name, 'phone':phone};
		var data=JSON.stringify(temp);
		// var url=`/ListMyOrderStat?name=${name}?phone=${phone}`;
		var url=`/ListMyOrderStat`;
		// console.log(url);
		callAjax(url,data);
		$("#StatusMessage").html($("#StatusMessage").html()+"<br>********************************<br> Hi "+name+"!    Welcome! <br> We are now looking for your order history<br>********************************<br>");
		$('#IndexStatusSection2 .row div').animate({ scrollTop: $('#StatusMessage').height() }, 700);
	});

});



var callAjax= function (urll,messageForGateway) {

			 $.ajax({
	        url: urll,
	        type: 'POST',
	        data: messageForGateway,
	        cache: false,
	        dataType: 'json',
	        processData: false, // Don't process the files
	        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
	        success: function(data, textStatus, jqXHR)
	        {
	        	console.log('test ' + "blah");
	            if(typeof data.error === 'undefined')
	            {
	            	// console.log('ajax success ' + "blah");
	             //    // Success so call function to process the form
	             //    submitForm(event, data);
	             //    // startListening();
	            }
	            else
	            {
	                // Handle errors here
	                console.log('ERRORS: ' + data.error);
	            }
	        },
	        statusCode: {
			    200: function() {
			      console.log('status 200 received2');
			      startListening();
			    }
			  },
	        error: function(jqXHR, textStatus, errorThrown)
	        {
	        } 
	    });
};

var startListening=function () {

			console.log('test: start');
			var refreshIntervalId2 = setInterval(function(){
				try {
					$.get("/status", function(statusMessage) {

						if (statusMessage!=null && statusMessage!=undefined && statusMessage.length>9) {
							console.log('Status message received: '+statusMessage);
							clearInterval(refreshIntervalId2);
							var temp=$("#StatusMessage").html();
							$("#StatusMessage").html(temp+statusMessage);

							$('#IndexStatusSection .row div').animate({ scrollTop: $('#StatusMessage').height() }, 700);
						} else {
							console.log('checking status');
						}
						console.log("Test refreshIntervalId2");
					});

				}
				catch(err) {
				    console.log("test: error: "+err.message);
				}

		}, 500);
	
}


