# Manufacturing-process-management

**A web client that allows clients to add orders using form or a xml file that is validated by the schema file already located in the server schema directory. The producers can view the orders and edit the status of each order to inform the completion of manufature. The completed orders are then visible to the delivery managers who can also change the status of the order to 'delivered' once the order is reach the destination address.**

## How to Run the Application

To run the application, please make sure you have all the dependencies installed from the prerequisite list

### Prerequisites

 * NodeJS
 * MySQL
 
### Running Locally

* Download and install the latest NodeJS. If NodeJS is already installed, 
continue from step 2. 
* Clone the repo in any local directory 
* Run command prompt(cmd) or terminal from that directory 
* In cmd, write 
  `npm install` 
and press enter to install all the dependencies
* Download and install MySQL Community edition. 
* Start MySQL server and create a database 'productiondb' and the necessary tables: customers, products and orders. Please follow the provided schema to create the tables easily. 
* After all dependencies have been installed and MySQL is set up as instructed, in cmd or terminal, type in the following command to run the server 
  ```node server.js```
* Once server is running, from any browser go to `http://localhost:8081` to add or view orders as client, go to `http://localhost:8081/manufacturer` as role manufacturer and `http://localhost:8081/delivery` as role deliverer.

_N.B: The schema files must be kept in the folder named schema. If no schema file 
name is mentioned in the xml file, it uses a default schema file called 
“order_details”_


 
## Author

* **[Abhijit Das](https://www.linkedin.com/in/abhijit-das-jd/)**


## License

This project is free to be used for any learning or educational purpose but cannot be used for commercial use without proper consent of the author.

## Acknowledgments

* Special thanks to Lecturer [Andrei Lobov](mailto:andrei.lobov@tut.fi) for his excellent lectures and guidance during the course Factory Informatics & Systems
* Gratitude to all my teachers, mentors, book writers and online article writers who provided me with the knowledge required for the project. 