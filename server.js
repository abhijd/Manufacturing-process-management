/**
 * Created by Abhi JD on 3/7/2017.
 */
var fs = require('fs');
var validator = require('xsd-schema-validator');
const path=require('path');
var express =   require("express");
var multer  =   require('multer');
var app         =   express();


//


var Message=null;
var SystemMessage=null;


//*****************************************
//***************File Handling*************
//*****************************************

var name="";

var storage =   multer.diskStorage({

    destination: function (req, file, callback) {
        callback(null, './uploads');
    },
    filename: function (req, file, callback) {
        // console.log("test: filename");
        var date = new Date();
        var day=date.getDate();
        var month=date.getMonth();
        var year=date.getFullYear();
        var h = date.getHours();
        var m=date.getMinutes();
        var s=date.getSeconds();
        name="";
        for(var i=0;i<file.originalname.length;i++){
            if(file.originalname[i]=="."){break;}else{
                name=name+file.originalname[i];
            }
        }

        filename=name+'_'+day+'-'+month+'-'+year+'_'+h+'-'+m+'-'+s+".xml";
        callback(null, filename );
    }
});

var upload = multer({ storage : storage, fileFilter: fileFilter }).single('xmlFile');


// the user uploads an order using xml file
app.post('/checkxml',function(req,res){

    console.log('checkxml reached');

    Message=null;
    SystemMessage=null;

    sendStatuss();

    upload(req,res,function(err) {
        console.log('testL: upload reached');
        if(err) {
            console.log(err);
            res.end(err.toString());
        }else {

            waitForMessage(res);
        }
    });
    res.sendStatus(200);
});

//*****************************************
//***************End of File Handling******
//*****************************************


app.use('/', express.static(   path.join(__dirname, 'public')  )  );


// The main page of the app is rendered

app.get('/',function(req,res){
    res.sendFile(__dirname + "/index.html");
});


//*****************************************
//***************Manufacturer Part********
//*****************************************


app.get('/manufacturer',function(req,res){
    res.sendFile(__dirname+"/manufacturer.html");

});

//*****************************************
//*********END of Manufacturer Part********
//*****************************************




//*****************************************
//************DeliveryPart****************
//*****************************************


var ProducerQuery=[];
ProducerQuery[0]=null;
ProducerQuery[1]="SELECT products.* FROM  products WHERE products.state='manufactured and shipped';";
ProducerQuery[2]="SELECT products.* FROM  products WHERE products.state='in_production';";
ProducerQuery[3]="SELECT customers.* FROM  customers, orders, products WHERE products.state='in_production' AND  products.orders_OrderID=orders.OrderID And orders.customers_CustomerID=customers.CustomerID GROUP BY CustomerID;";
ProducerQuery[4]="SELECT customers.* From customers, orders WHERE ADDDATE(CURDATE(), -7)<DATE(orders.date) AND orders.customers_CustomerID=customers.CustomerID GROUP BY customers.CustomerID;";

var DelPass='A2FIS';


app.get('/delivery',function(req,res){

    res.sendFile(__dirname + "/delivery.html");
});

var DelQuery="SELECT products.ProductID AS ID, products.name AS 'Product Name', customers.address AS 'Delivery Address', customers.phone AS 'Contact', products.state AS 'Current State' FROM  products, orders, customers WHERE products.state='READY FOR DELIVERY' AND products.orders_OrderID=orders.OrderID AND orders.customers_CustomerID=customers.CustomerID;";



app.post('/deliveryquery', function ( req, res) {
    res.sendStatus(200);

    var site=req.url;

    console.log("TEST URL: "+ site);
    // console.log('name: '+req.query.name);
    // console.log('name: '+req.query.phone);
    var body = [];
    req.on('data', function(chunk) {
        body.push(chunk);

        var DelFormData = JSON.parse(body.toString());
        console.log('ProFormData: ' + JSON.stringify(DelFormData) );
        console.log('ProFormData keys: '+Object.keys(DelFormData) );

        if(DelPass===DelFormData.DelPass && DelFormData.button==false){
            connection.query(DelQuery, function (error, results, fields) {
                if (error){
                    console.log("***Error in the  query:*** \n"+"\n\n****Error Details: ****\n",error);
                    Message="Something went wrong, Please, contact the damn App developer";
                }else{
                    console.log('result test: ', typeof(results), results.length  );
                    var buttonRequestURL="/delStatChange";
                    if(results.length>0){
                        SQLresult2TableWithButton(results,buttonRequestURL);
                    }else{
                        Message='No Products to deliver at the moment.';
                    }
                }
            });
        }else if(DelPass===DelFormData.DelPass && DelFormData.button==true){
            var DelQuery2="UPDATE `productiondb`.`products` SET products.state='delivered' WHERE `ProductID` = "+DelFormData.QueryOpt+";";
            connection.query(DelQuery2, function (error, results, fields) {
                if (error){
                    console.log("***Error in the  query:*** \n"+"\n\n****Error Details: ****\n",error);
                    Message="Something went wrong, Please, contact the damn App developer";
                }else{
                    console.log('result test: ', typeof(results), results.length  );
                    var buttonRequestURL="/delStatChange";
                    connection.query(DelQuery, function (error, results, fields) {
                        if (error){
                            console.log("***Error in the  query:*** \n"+"\n\n****Error Details: ****\n",error);
                            Message="Something went wrong, Please, contact the damn App developer";
                        }else{
                            console.log('result test: ', typeof(results), results.length  );
                            var buttonRequestURL="/delStatChange";
                            if(results.length>0){
                                SQLresult2TableWithButton(results,buttonRequestURL);
                            }else{
                                Message='No Products to deliver at the moment.';
                            }

                        }
                    });

                }
            });

        }else{
            Message="<br>&#9760;&#9760;&#9760;&#9760; Wrong Password!! &#9760;&#9760;&#9760;&#9760;<br>";
        };

        sendStatuss();

    });

});

function SQLresult2TableWithButton(results, butStatChange) {
    console.log(butStatChange);
    var keys=Object.keys(results[0]);
    var tempMessage="<br><br> Your Requested Data are listed in the table below. <br>&#9787;&#9787;&#9787;&#9787;&#9787;<br> ENJOY <br>&#9787;&#9787;&#9787;&#9787;<br><br>";
    tempMessage=tempMessage+"<table> <tr>";
    for(var i=0;i<keys.length;i++){

        tempMessage=tempMessage+"<th>"+keys[i]+"</th>";
        if(i==keys.length-1){
            tempMessage=tempMessage+"<th>Change Delivery Status</th>";
            tempMessage=tempMessage+"</tr>";
            for(var j=0;j<results.length;j++){
                tempMessage=tempMessage+"<tr>";
                for(var k=0;k<keys.length;k++){
                    tempMessage=tempMessage+"<td>"+results[j][keys[k]] +"</td>";
                    if(k==keys.length-1){
                        tempMessage=tempMessage+"<td><button class='btn btn-primary' onclick='DelStatButtonClicked("+results[j][keys[0]] +")'>Change</button></td>";
                        tempMessage=tempMessage+"</tr>";
                        if(j==results.length-1){
                            tempMessage=tempMessage+"</table>";
                            console.log(tempMessage);
                            Message=tempMessage;
                        }
                    }
                };

            }
        }
    }

}


app.post('/ListMyOrderStat', function ( req, res) {
    res.sendStatus(200);
    var site=req.url;
    console.log("TEST REQ URL FROM ListMy: "+ site);
    // console.log('name: '+req.query.name);
    // console.log('name: '+req.query.phone);
    var body = [];
    req.on('data', function(chunk) {
        body.push(chunk);

        var StatFormData = JSON.parse(body.toString());
        console.log('StatFormData: ' + JSON.stringify(StatFormData) );
        console.log('StatFormData keys: '+Object.keys(StatFormData) );
        // takeOrder(FormData);
        CustomerOrderHistory(StatFormData.name,StatFormData.phone);
        Message=null;
        sendStatuss();

    });

});

function CustomerOrderHistory(name,phone) {

    connection.query("SELECT CustomerID FROM `productiondb`.`customers` WHERE name='"+name+"' AND phone='"+phone+"';", function (error, results, fields) {
        if (error){
            console.log("***Error in the  query:*** \n"+"\n\n****Error Details: ****\n",error);
        }else{
            console.log('result test: ', typeof(results), results.length  );
            if(results.length==1){
                console.log("****CustomerOrderHistory: Customer found*****");
                ListOrders(results[0].CustomerID);


            }else if(results.length==0){
                console.log("****CustomerOrderHistory: Customer not found*****");
                Message="<br>*********<br>Hi "+name+", we couldn't find you in our list of customers.<br>*Please check that you have correctly entered your name and phone number*";
            }
        }
    });
}

function ListOrders (CustomerID) {

    var query=
        "SELECT orders.refID AS 'Order ID',"
        +"    DATE(orders.date) AS 'Order Date ', "
        +"    products.name AS 'Product Name',"
        +"    products.quantity AS 'Quantity',"
        +"    products.frameDesign AS 'Frame Type', products.frameColor AS 'Frame Color',"
        +"    products.screenDesign AS 'Screen Type', products.screenColor AS 'Screen Color',"
        +"    products.keypadDesign AS 'Keypad Type', products.keypadColor AS 'Keypad Color',"
        +"    products.state AS 'State' "
        +"FROM orders, products, customers "
        +"WHERE `customers`.`CustomerID` = `orders`.`customers_CustomerID` "
        +"AND `orders`.`OrderID` = `products`.`orders_OrderID` "
        +"AND customers.CustomerID = "+CustomerID
        +" ORDER BY DATE(orders.date) DESC;";
    console.log(query);

    connection.query(query, function (error, results, fields) {
        if (error){
            console.log("***Error in the the following query:*** \n",query,"\n\n****Error Details: ****\n",error);
            Message="Something went wrong, we are extremely sorry. Please contact customer care ASAP";
        }else{
            // console.log('The solution is: ', results, typeof(results.insertId), results.insertId  );
            for(var i=0;i<results.length;i++){
                console.log(`Row ${i} : `+ JSON.stringify( results[i] ) );
            }

            SQLresult2Table(results);
        }
    });

}

function SQLresult2Table(results) {
    var keys=Object.keys(results[0]);
    var tempMessage="<br><br> Your Requested Data are listed in the table below. <br>&#9787;&#9787;&#9787;&#9787;&#9787;<br> ENJOY <br>&#9787;&#9787;&#9787;&#9787;<br><br>";
    tempMessage=tempMessage+"<table style='width:100%;border:dashed green;'> <tr style='border:dotted green;'>";
    for(var i=0;i<keys.length;i++){
        if(!i){
            // tempMessage=tempMessage+"<table><tr>";
        };
        tempMessage=tempMessage+"<th style='border:dotted green;'>"+keys[i]+"</th>";
        if(i==keys.length-1){
            tempMessage=tempMessage+"</tr>";
            for(var j=0;j<results.length;j++){
                tempMessage=tempMessage+"<tr style='border:dotted green;'>";
                for(var k=0;k<keys.length;k++){
                    tempMessage=tempMessage+"<td style='border:dotted green;'>"+results[j][keys[k]] +"</td>";
                    if(k==keys.length-1){
                        tempMessage=tempMessage+"</tr>";
                        if(j==results.length-1){
                            tempMessage=tempMessage+"</table>";
                            console.log(tempMessage);
                            Message=tempMessage;
                        }
                    }


                };

            }
        }
    }

}



app.post('/peeppeepOrderTule',function (req,res) {
    console.log("/peeppeepOrderTule Reached");
    // console.log("Body:********\n",req.body);
    // console.log('req*********',req);
    res.sendStatus(200);

    var body = [];
    req.on('data', function(chunk) {
        body.push(chunk);

        var FormData = JSON.parse(body.toString());
        console.log('abc0: ' + JSON.stringify(FormData) );
        console.log('abc: '+Object.keys(FormData.order_details) );
        takeOrder(FormData);

    });

    Message=null;
    sendStatuss();
    // waitForMessage(res)


});



function waitForMessage(res){
    if (Message==null){
        setTimeout(function () {
            waitForMessage(res);
            console.log('testL: waiting');
        },3000);
    }else{
        if (SystemMessage!="404") {
            // res.end(Message + "\n\n" + SystemMessage);
            console.log('test: sendStatus');


        }else{
            res.status(404).end("HTTP 404\n\n"+Message);
            process.exit(0);
        }
    }
}


/************************************************************
 File type is checked to only allow xml file type
 ***********************************************************/

function fileFilter (req, file, cb) {

    console.log('testL: filefilter');

    if(file.mimetype=="text/xml"){
        console.log("Received new request for checking xml file: "+  file.originalname );
        cb(null, true);
        console.log('testL: filefilter, check1');

        checkFileExist(filename);
    }
    else{
        console.log("The file: "+file.originalname+" has an invalid file type of : "+file.mimetype+" . Please upload a xml file");
        cb(new Error('Invalid file type, file not uploaded.'));
        Message="The file: "+file.originalname+" has an invalid file type of : "+file.mimetype+" . Please upload a xml file";
        SystemMessage="";
        console.log('testL: filefilter, check2');
    }
}

/************************************************************
 Recurring Function to make sure upload of xml is complete
 ***********************************************************/
function checkFileExist(filename){
    fs.access('./uploads/'+filename, fs.R_OK, function (err) {
        if (err){
            setTimeout(function () {
                checkFileExist(filename);
            },100);

        }else{
            setTimeout(function () {
                readXML(filename);
            },500);

        }
    });
}

function readXML(filename){

    fs.open('./uploads/'+filename, 'r', function(err, fd)  {

        xml = fs.readFileSync('./uploads/'+filename);
        xmlStr = fs.readFileSync('./uploads/'+filename).toString();
        // console.log("*****Test xml check: ***** \n"+xmlStr+"\n\n***filename: "+filename);

        /*******************************************
         Obtaining the name of the schema file (if any)
         ********************************************/

        xmlStrTemp = xmlStr.replace(/<!--[\s\S]*?-->/g, '');
        // console.log("*****Test xml check2: ***** \n"+xmlStrTemp);
        var n = xmlStrTemp.includes("xsi:schemaLocation") || xmlStrTemp.includes("xsi:noNamespaceSchemaLocation");
        if(n){
            var k = xmlStrTemp.search(".xsd\"");
            if (k<0){
                var k = xmlStrTemp.search(".xsd\'");
            }
            for (var i = k; i >1; i--) {
                if (xmlStrTemp[i]=="\"" || xmlStrTemp[i]=="\'" || xmlStrTemp[i]==" " ) {
                    var xsdSchema=xmlStrTemp.slice(i+1,k+4);
                    console.log("Will attemp to read schema file:  "+xsdSchema);
                    break;
                }
            }
        }else{

            var xsdSchema='order_details.xsd';
            console.log("Schema not specified in xml file, Will attempt to read default schema file:  "+xsdSchema);
        }

        /*******************************************
         Checking if xsd file exists
         ********************************************/

        fs.access("./schemas/"+xsdSchema, fs.R_OK, function (err) {
            if (err){
                console.log("Schema file "+xsdSchema+ " not found, validation incomplete");
                console.log(err);
                Message="<br><br>********************<br><br>***Schema file "+xsdSchema+ " not found, validation incomplete***";
                SystemMessage="404";

            }else{
                console.log("Schema file found, validating now.......");

                /*******************************************
                 Validating the xml file using xsdSchema file
                 ********************************************/
                validator.validateXML(xmlStr, "./schemas/"+xsdSchema, function(err, result) {
                    if (err) {
                        console.log("\n ***Error during xml validation***");
                        console.log(err);

                        Message="<br><br><br>***************<br>***Error during xml validation***";
                        SystemMessage=err;
                    }
                    if(result.valid) {
                        console.log("Validation Successful. Inserting in Database \n ");
                        // console.log(xmlStr);
                        // var TempMessage="The file "+name+".xml"+" is valid \n\n ";
                        SystemMessage=xmlStr;

                        var convertedXML0=convertxml(xmlStr);
                        var convertedXML=JSON.parse(convertedXML0);
                        takeOrder(convertedXML );


                    }
                });
            }
        });
    });
}


function takeOrder(convertedXML ) {
    var orderedby=convertedXML.order_details.ordered_by;


    var items=convertedXML.order_details.ordered_items.item;
    var n0=0;
    var n1=items.length;

    connection.query("SELECT CustomerID FROM `productiondb`.`customers` WHERE name='"+orderedby.name._text+"' AND phone='"+orderedby.phone._text+"';", function (error, results, fields) {
        if (error){
            console.log("***Error in the  query:*** \n"+"\n\n****Error Details: ****\n",error);
        }else{
            console.log('result test: ', typeof(results), results.length  );
            if(results.length==1){
                console.log("****Existing Customer*****");

                ExistingCustomer(results[0].CustomerID);
            }else if(results.length==0){
                console.log("****New Customer*****");

                NewCustomer();
            }
        }
    });

function ExistingCustomer(CustID) {
    var TempMessage="<br><br><br>-----------------------------------"
        +"<br><br> Hi "+orderedby.name._text+", you are one of our existing customer. <br> ";

    connection.query("SELECT OrderID FROM `productiondb`.`orders` WHERE refID='"+convertedXML.order_details.order_id._text+"' AND customers_CustomerID="+CustID+";" , function (error, results, fields) {
        if (error){
            console.log("***Error in the  query:*** \n"+"\n\n****Error Details: ****\n",error);
        }else{
            console.log('result test: ', typeof(results), results.length  );
            if(results.length==1){
                console.log("****Existing Order*****");

                Message=TempMessage+"<br> Your order with ID: "+convertedXML.order_details.order_id._text+" already exists.";
            }else if(results.length==0){
                console.log("****New Order*****");
                var inOrder=insertOrderInDB(convertedXML.order_details.order_id._text,
                    convertedXML.order_details.order_date._text,
                    CustID);
                connection.query(inOrder, function (error, results, fields) {
                    if (error){
                        console.log("***Error in the the following query:*** \n"+"\n\n****Error Details: ****\n",error);
                        TempMessage=TempMessage+"<br>There was an error in processing your order!!\n";
                        // connection.end();
                        // console.log('****Connection Closed****');
                    }else{
                        console.log('The solution is: ', results, typeof(results.insertId), results.insertId  );
                        TempMessage=TempMessage+"<br> You order with OrderID "+convertedXML.order_details.order_id._text+" has been added<br>";

                        LastOrderID=results.insertId;
                        var timer1=setInterval(function(){
                            TempMessage=TempMessage+"<br> The Product "+items[n0].name._text+" has been added to your order";
                            insertProductInDB(items[n0].name._text,
                                items[n0].quantity._text,
                                items[n0].frame.design._text,
                                items[n0].frame.color._text,
                                items[n0].screen.design._text,
                                items[n0].screen.color._text,
                                items[n0].keypad.design._text,
                                items[n0].keypad.color._text,
                                'ordered',
                                LastOrderID);
                            n0++;
                            if (n0==n1) {
                                Message=TempMessage;
                                clearInterval(timer1);
                                // connection.end();
                                // console.log('****Connection Closed****');
                            }
                        }, 400);

                    }
                });


            }
        }
    });
}


function NewCustomer() {
    var TempMessage="<br><br><br>-----------------------------------"
        +"<br>Hi "+orderedby.name._text+", you are a new customer. Welcome! <br><br>";

    var line=orderedby.address.line1._text+' ';
    if (orderedby.address.line2!=undefined){
        line=orderedby.address.line1._text+' '+orderedby.address.line2._text;
    }

    var inCust=insertCustomerInDB(orderedby.name._text,
        orderedby.email._text,
        line+' '+orderedby.address.city._text+' '+orderedby.address.country._text,
        orderedby.phone._text
    );


    connection.query(inCust, function (error, results, fields) {
        if (error){
            console.log("***Error in the query:*** \n"+"\n\n****Error Details: ****\n",error);
            // connection.end();
            // console.log('****Connection Closed****');

        }else{
            console.log('The solution is: ', results, typeof(results.insertId), results.insertId  );
            LastCustID=results.insertId;


            var inOrder=insertOrderInDB(convertedXML.order_details.order_id._text,
                convertedXML.order_details.order_date._text,
                LastCustID);



            connection.query(inOrder, function (error, results, fields) {
                if (error){
                    console.log("***Error in the the following query:*** \n"+"\n\n****Error Details: ****\n",error);
                    // connection.end();
                    // console.log('****Connection Closed****');
                }else{
                    TempMessage=TempMessage+"<br> You order with OrderID "+convertedXML.order_details.order_id._text+" has been added <br>";
                    console.log('The solution is: ', results, typeof(results.insertId), results.insertId  );
                    LastOrderID=results.insertId;
                    var timer1=setInterval(function(){
                        TempMessage=TempMessage+"<br> The Product "+items[n0].name._text+" has been added to your order";
                        insertProductInDB(items[n0].name._text,
                            items[n0].quantity._text,
                            items[n0].frame.design._text,
                            items[n0].frame.color._text,
                            items[n0].screen.design._text,
                            items[n0].screen.color._text,
                            items[n0].keypad.design._text,
                            items[n0].keypad.color._text,
                            'ordered',
                            LastOrderID);
                        n0++;
                        if (n0==n1) {
                            Message=TempMessage;
                            clearInterval(timer1);
                            console.log("Message: ", Message);
                            // connection.end();
                            // console.log('****Connection Closed****');
                        }
                    }, 400);

                }
            });

        }
    });


}//jjjjjjjjjjjjjjjj

}





// ******************************************
//******************SQL PART*****************
//*******************************************

var mysql      = require('mysql');
var connection = mysql.createConnection({
    host     : 'localhost',
    port     : '3306',
    user     : 'root',
    password : '1234',
    database : 'productiondb',
    charset  : 'UTF8_GENERAL_CI',
});

// connection.connect();
//     connection.query("INSERT INTO `productiondb`.`customers`(`name`,`email`,`address`,`phone`)VALUES('some weird name','abhi@abhi.com','street address 1 street 2 Tampere Finland','0231145');", function (error, results, fields) {
//       if (error){
//         console.log("***Error in the the following query:*** \n",SQLquery,"\n\n****Error Details: ****\n",error);
//       }
//       console.log('The solution is: ', results.insertId);
//     });
//     connection.end();






// var InsertOrUpdateDB= function(SQLquery){
//
//
// };

var insertCustomerInDB=function (name, email, address, phone) {
    var SQLquery="INSERT INTO `productiondb`.`customers`(`name`,`email`,`address`,`phone`)VALUES(\'"
        +name+"\',\'"
        +email+"\',\'"
        +address+"\',\'"
        +phone
        +"\');";
    console.log('\n\n***Trying the query: \n\n'+SQLquery);
    return SQLquery;
};

var insertOrderInDB=function (refID, date, customerFK) {
    var SQLquery="INSERT INTO `productiondb`.`orders`(`refID`,`date`,`customers_CustomerID`)VALUES(\'"
        +refID+"\',\'"
        +date+"\',\'"
        +customerFK
        +"\');";
    console.log('\n\n***Trying the query: \n\n'+SQLquery);
    return SQLquery;
};

var insertProductInDB=function (name,quantity,fd,fc,sd,sc,kd,kc,state,orderLastID) {
    var query="INSERT INTO `productiondb`.`products`"
        +"(`name`,`quantity`,`frameDesign`,`frameColor`,`screenDesign`,`screenColor`,`keypadDesign`,`keypadColor`,`state`,`orders_OrderID`)"
        +"VALUES(\'"
        +name+"\',\'"
        +quantity+"\',\'"
        +fd+"\',\'"
        +fc+"\',\'"
        +sd+"\',\'"
        +sc+"\',\'"
        +kd+"\',\'"
        +kc+"\',\'"
        +state+"\',\'"
        +orderLastID
        +"\');";
    console.log('\n\n***Trying the query: \n\n'+query);
    connection.query(query, function (error, results, fields) {
        if (error){
            console.log("***Error in the the following query:*** \n",query,"\n\n****Error Details: ****\n",error);
        }else{
            console.log('The solution is: ', results, typeof(results.insertId), results.insertId  );
            // LastProductID=results.insertId;
        }
    });
};


//*********END of SQL PART*******************



// ******************************************
//******************XML-Parsing PART**********
//*******************************************
var convert = require('xml-js');

var convertxml=function (xmlString) {
    return convert.xml2json(xmlString, {compact: true, spaces: 4});
};

//*****************END of XML Parsing Part****




app.listen(8081,function(){
    console.log("Server Listening at http://127.0.0.1:8081");
    console.log("***********************************\n");
    console.log("Also Server Listening at http://127.0.0.1:8081/producer");
    console.log("***********************************\n");
});


var sendStatuss= function () {
    app.get("/status", function(req, res) {

        res.send(Message);
        res.end();
        console.log("status url reached0");
        console.log(req.query);
    });


};


// ListOrders(3);

